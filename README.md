# scratch3_text2speech

In Scratch 2.0, we can use a variable to convert user entered text in to a speech bubble.
  
For example.

![Cat1](https://github.com/zleap/scratch3_text2speech/blob/master/cat-sprite.png)  

  Using the code 
  
![Text2speechbubble](https://github.com/zleap/scratch3_text2speech/blob/master/data2text.png)
  
With Scratch 3.0, the Scratch team have added **text to speech**. We can now make characters (sprites) talk with sound.

Firstly we need to import the ‘Text2Speech’ module.

On the **Bottom left** click on
  ![Importmodulesbutton](https://github.com/zleap/scratch3_text2speech/blob/master/import-modules.png)

To bring up the modules page and click on:

![s3modules](https://github.com/zleap/scratch3_text2speech/blob/master/s3-modules.png)
  
You will be taken back to the main screen.

Now create the program we looked at earlier.

  ![s3txttospeechbubble](https://github.com/zleap/scratch3_text2speech/blob/master/s3-d2t.png)

When you run this you get a text input box. 
 
![textinputbox](https://github.com/zleap/scratch3_text2speech/blob/master/txtinputbox.png)

So enter some text. For example **hello**

Now lets make our sprite say what we typed in to this. Use

![Sayblock2](https://github.com/zleap/scratch3_text2speech/blob/master/sayblock2.png)
  and
  ![amswer block](https://github.com/zleap/scratch3_text2speech/blob/master/answer.png)

to make :

![sayblock](https://github.com/zleap/scratch3_text2speech/blob/master/say-block.png)
  

The final program should look like:

  ![txt2speechfinal](https://github.com/zleap/scratch3_text2speech/blob/master/txt2speech-final.png)


Have fun. You can change the type of voice and also the language.

Created by Paul Sutton https://personaljournal.ca/paulsutton/scratch-project-2 on December 9th 2018

![Download as pdf](https://github.com/zleap/scratch3_text2speech/blob/master/text2speech.pdf)

![cc-logo](https://github.com/zleap/scratch3_text2speech/blob/master/88x31.png)
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTU1NzY4NjM3NF19
-->
